## SPDX-License-Identifier: BSD-3-Clause
## Copyright (c) 2020 Rafal Kozik

# 50 MHz 
create_clock -name {clk} -period 20.000 -waveform { 0.000 10.000 } [get_ports { clk }]

/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module sender (
    input wire clk,
    input wire rst,
    input wire [1:0]rxd,
    input wire crs_dv,
	 output logic [1:0]txd,
    output logic tx_en
);
    logic [28:0] cnt;

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            cnt <= '0;
        else
            cnt <= cnt + 29'd1;

    mac_tx #(
        .DATA_SIZE(8),
        .BUF_SIZE(512)
    ) tx (
        .clk(clk),
        .rst(rst),
        .data_valid(1'b1),
        .data(8'd48 + cnt[27:25]),
        .data_addr(9'd59),
        .pkt_size(9'd60),
        .pkt_trigger(cnt[25]),
        .tx_d(txd),
        .tx_en(tx_en));

endmodule

`default_nettype wire

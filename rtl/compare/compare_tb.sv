/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module compare_tb;
    logic clk;
    logic rst;

    logic [1:0]d_rx_mem[511:0];
    logic d_en_mem[511:0];
    logic [1:0]d_rx;
    logic d_en;

    initial begin
        clk = 1'b0;
        forever #10 clk = !clk;
    end

    initial begin
        rst = 1'b0;
        #40 rst = 1'b1;
    end

    initial begin
        $readmemh("d_rx.mem", d_rx_mem);
        $readmemh("crs_dv.mem", d_en_mem);
        d_rx = '0;
        d_en = '0;
        for (int i = 0; i < 10; i++)
            @(posedge clk);
        for (int i = 0; i < $size(d_en_mem); i++) begin
            d_rx = d_rx_mem[i];
            d_en = d_en_mem[i];
            @(posedge clk);
        end
        $stop();
    end

    compare dut (
        .clk(clk),
        .rst(rst),
        .rx_d(d_rx),
        .rx_en(d_en),
        .data()
    );

endmodule

`default_nettype wire

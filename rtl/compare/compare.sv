/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module compare (
    input wire clk,
    input wire rst,
    input wire [1:0]rx_d,
    input wire rx_en,
    output logic [7:0]data
);
    parameter logic [0:5][3:0][1:0]mac = {8'h2, 8'h0, 8'h0, 8'h0, 8'h0, 8'h0};

    logic [1:0]rx_d_d;
    logic [1:0]rx_en_d;
    logic [4:0]cnt;
    logic [3:0][1:0]data_in;

    enum logic [2:0] {
        IDLE,
        PREAMB,
        MAC_DST,
        MAC_SRC,
        ETHER_TYPE,
        DATA,
        SAVE,
        TO_END
	} state;

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            rx_en_d <= 'b0;
        else
            rx_en_d <= {rx_en_d[0], rx_en};

    always_ff @(posedge clk)
        rx_d_d <= rx_d;

    always_ff @(posedge clk)
        if (state == PREAMB)
            cnt <= '0;
        else if(cnt == 5'd23 && (state == MAC_DST || state == MAC_SRC))
            cnt <= 3'd0;
        else if(cnt == 5'd7 && state == ETHER_TYPE)
            cnt <= 3'd0;
        else
            cnt <= cnt + 3'd1;

    always_ff @(posedge clk)
        if (state == DATA)
            data_in[cnt] <= rx_d_d;

    always_ff @(posedge clk)
        if (state == SAVE)
            data <= data_in;

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            state <= IDLE;
        else begin
            case(state)
                IDLE: state <= (rx_d_d == 2'd1 && rx_en_d[0]) ? PREAMB : IDLE;
                PREAMB:
                    case ({rx_en_d[0], rx_d_d})
                        3'b101: state <= PREAMB;
                        3'b111: state <= MAC_DST;
                        default: state <= TO_END;
                    endcase
                MAC_DST: begin
                    if (mac[cnt[4:2]][cnt[1:0]] == rx_d_d)
                        state <= (cnt == 5'd23) ? MAC_SRC : MAC_DST;
                    else
                        state <= TO_END;
                end
                MAC_SRC: state <= (cnt == 5'd23) ? ETHER_TYPE : MAC_SRC;
                ETHER_TYPE: state <= (cnt == 5'd7) ? DATA : ETHER_TYPE;
                DATA: state <= (cnt == 5'd3) ? SAVE : DATA;
                SAVE: state <= TO_END;
                TO_END: state <= (|rx_en_d) ? TO_END : IDLE;
            endcase
        end

endmodule

`default_nettype wire

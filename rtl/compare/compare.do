#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog compare.sv
vlog compare_tb.sv

vsim -novopt work.compare_tb

add wave -position end sim:/compare_tb/clk
add wave -position end sim:/compare_tb/rst
add wave -position end sim:/compare_tb/d_en
add wave -hex -position end sim:/compare_tb/d_rx

add wave -position end sim:/compare_tb/dut/rx_en_d\[0\]
add wave -hex -position end sim:/compare_tb/dut/rx_d_d
add wave -position end sim:/compare_tb/dut/state
add wave -unsigned -position end sim:/compare_tb/dut/cnt
add wave -unsigned -position end sim:/compare_tb/dut/data_in

add wave -unsigned -position end sim:/compare_tb/dut/data

run -all
wave zoom full

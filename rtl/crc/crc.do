#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog crc.sv
vlog crc_tb.sv

vsim -novopt work.crc_tb

add wave -position end sim:/crc_tb/clk
add wave -position end sim:/crc_tb/rst
add wave -position end sim:/crc_tb/sop
add wave -position end sim:/crc_tb/eop
add wave -hex -position end sim:/crc_tb/din
add wave -hex -position end sim:/crc_tb/dut/pol
add wave -hex -position end sim:/crc_tb/dut/c
add wave -hex -position end sim:/crc_tb/dut/c_r
add wave -hex -position end sim:/crc_tb/dut/state
add wave -hex -position end sim:/crc_tb/dut/cnt
add wave -hex -position end sim:/crc_tb/dut/d_out
add wave -hex -position end sim:/crc_tb/dut/sop_out
add wave -hex -position end sim:/crc_tb/dut/eop_out
add wave -hex -position end sim:/crc_tb/valid
add wave -hex -position end sim:/crc_tb/err

run -all
wave zoom full

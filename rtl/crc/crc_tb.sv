/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module crc_tb;
    logic clk;
    logic rst;
    logic [1:0]din;
    logic sop, eop;
    logic [1:0]pkt[255:0];
    logic [1:0]d_out;
    logic [1:0]dd[1:0];
    logic sop_out, eop_out;
    logic valid, err;

    initial begin
        clk = 1'b0;
        forever #10 clk = !clk;
    end

    initial begin
        rst = 1'b0;
        #40 rst = 1'b1;
    end

    initial begin
        $readmemh("p64.mem", pkt);
        {sop, eop} = '0;
        #40;
        @(posedge clk);
        sop = 1'b1;
        for (int i = 0; i < ($size(pkt)); i++) begin
            din = pkt[i];
            eop = (i == $size(pkt) - 17);
            @(posedge clk);
            sop = 1'b0;
        end
        eop = 1'b0;
        for (int i = 0; i < 16; i++) begin
            din = '1;
            @(posedge clk);
        end        
        for (int i = 0; i < 10; i++)
            @(posedge clk);
        $stop;
    end

    crc dut (
        .clk(clk),
        .rst(rst),
        .d(din),
        .sop(sop),
        .eop(eop),
        .d_out(d_out),
        .sop_out(sop_out),
        .eop_out(eop_out));

    always_ff @(posedge clk) begin
        dd[0] <= din;
        for (int i = 1; i < $size(dd); i++)
            dd[i] <= dd[i-1];
    end

    always_ff @(posedge clk) begin
        if (sop_out)
            valid <= 1'b1;
        if (eop_out)
            valid <= 1'b0;
        if (sop_out || valid)
            err <= err | (dd[1] != d_out);
        if (!rst) begin
            {err, valid} <= '0;
        end
    end

    endmodule

`default_nettype wire

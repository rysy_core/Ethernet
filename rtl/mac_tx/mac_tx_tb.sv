/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module mac_tx_tb;
    logic clk;
    logic rst;

    logic [8:0]data_addr;
    logic [8:0]pkt_size;
    logic [7:0]data;
    logic [7:0]pkt[59:0];
    logic data_valid, pkt_trigger;

    logic [1:0]rgmii[287:0];
    logic [1:0]tx_d;
    logic tx_en, err;
    int out_i;

    initial begin
        clk = 1'b0;
        forever #10 clk = !clk;
    end

    initial begin
        rst = 1'b0;
        #40 rst = 1'b1;
    end

    initial begin
        //$readmemh("p64.mem", dut.pkt_mem.ram);
        // $readmemh("p64.mem", pkt);
        pkt_size = $size(pkt);
        data_valid = 1'b0;
        pkt_trigger = 1'b0;
        // #50;
        // @(posedge clk);
        // data_valid = 1'b1;
        // for (int i = 0; i < $size(pkt); i++) begin
        //     data = pkt[i];
        //     data_addr = i;
        //     @(posedge clk);
        // end
        // data_valid = 1'b0;
        for (int i = 0; i < 10; i++)
            @(posedge clk);
        pkt_trigger = !pkt_trigger;
        for (int i = 0; i < (4*$size(pkt)+80); i++)
            @(posedge clk);
        $stop();
    end

    mac_tx #(
        .DATA_SIZE(8),
        .BUF_SIZE(512)
    ) dut (
        .clk(clk),
        .rst(rst),    
        .data_valid(data_valid),
        .data(data),
        .data_addr(data_addr),    
        .pkt_size(pkt_size),
        .pkt_trigger(pkt_trigger),    
        .tx_d(tx_d),
        .tx_en(tx_en));

    initial begin
        $readmemh("p64_rgmii.mem", rgmii);
    end

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            err <= '0;
        else if(tx_en) begin
            out_i <= out_i + 1;
            err <= tx_d != rgmii[out_i];
        end

endmodule

`default_nettype wire

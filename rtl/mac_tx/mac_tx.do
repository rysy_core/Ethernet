#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../crc/crc.sv
vlog mixed_width_ram.sv
vlog mac_tx.sv
vlog mac_tx_tb.sv

vsim -novopt work.mac_tx_tb

add wave -position end sim:/mac_tx_tb/clk
add wave -position end sim:/mac_tx_tb/rst
add wave -hex -position end sim:/mac_tx_tb/dut/data
add wave -hex -position end sim:/mac_tx_tb/dut/data_addr
add wave -position end sim:/mac_tx_tb/dut/data_valid
add wave -hex -position end sim:/mac_tx_tb/dut/pkt_size
add wave -hex -position end sim:/mac_tx_tb/dut/pkt_trigger

add wave -position end sim:/mac_tx_tb/dut/state
add wave -position end sim:/mac_tx_tb/dut/start
add wave -hex -position end sim:/mac_tx_tb/dut/preamb_cnt
add wave -position end sim:/mac_tx_tb/dut/select_data_d
add wave -hex -position end sim:/mac_tx_tb/dut/d_preamb
add wave -position end sim:/mac_tx_tb/dut/mem_start
add wave -hex -position end sim:/mac_tx_tb/dut/raddr
add wave -hex -position end sim:/mac_tx_tb/dut/d_mem
add wave -position end sim:/mac_tx_tb/dut/sop
add wave -position end sim:/mac_tx_tb/dut/eop
add wave -hex -position end sim:/mac_tx_tb/dut/d_crc
add wave -position end sim:/mac_tx_tb/dut/add_crc/sop_out
add wave -position end sim:/mac_tx_tb/dut/eop_crc

add wave -hex -position end sim:/mac_tx_tb/dut/tx_d
add wave -position end sim:/mac_tx_tb/dut/tx_en
add wave -position end sim:/mac_tx_tb/err

run -all
wave zoom full

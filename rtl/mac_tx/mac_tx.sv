/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module mac_tx #(
    DATA_SIZE = 8,
    BUF_SIZE = 512,
    L_BUF_SIZE = $clog2(BUF_SIZE)
) (
    input wire clk,
    input wire rst,

    input wire data_valid,
    input wire [DATA_SIZE-1:0]data,
    input wire [L_BUF_SIZE-1:0]data_addr,

    input wire [L_BUF_SIZE-1:0]pkt_size,
    input wire pkt_trigger,

    output logic [1:0]tx_d,
    output logic tx_en
);
    parameter R_ADDR = $clog2(BUF_SIZE * DATA_SIZE / 2);

    enum logic {
        IDLE,
        SEND
	} state;
    logic [R_ADDR-1:0] raddr;
    logic [1:0] d_mem;
    logic [1:0] d_crc;
    logic [1:0] d_preamb;
    logic eop_crc, pkt_trigger_d, start, eop_crc_d;
    logic select_data, select_data_d, sop, eop, mem_start;
    logic mem_start_t, mem_start_t_d;
    logic [5:0] preamb_cnt;

    mixed_width_ram #(
        .WORDS(BUF_SIZE),
        .RW(2),
        .WW(DATA_SIZE)
    ) pkt_mem (
        .clk(clk),
        .we(data_valid),
        .waddr(data_addr),
        .wdata(data),
        .raddr(raddr),
        .q(d_mem));

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            pkt_trigger_d <= '0;
        else
            pkt_trigger_d <= pkt_trigger;

    assign start = pkt_trigger_d != pkt_trigger;

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            state <= IDLE;
        else case (state)
            IDLE: state <= start ? SEND : IDLE;
            SEND: state <= eop_crc ? IDLE : SEND;
            default: state <= IDLE;
        endcase

    always_ff @(posedge clk)
        if (state == IDLE)
            preamb_cnt <= '0;
        else if (preamb_cnt[5] == 1'b0)
            preamb_cnt <= preamb_cnt + 1'b1;

    always_ff @(posedge clk)
        d_preamb <= preamb_cnt[5] ? 2'd3 : 2'd1;

    always_ff @(posedge clk)
        if (state == IDLE)
            raddr <= '0;
        else if (mem_start)
            raddr <= raddr + 1'b1;

    always_ff @(posedge clk) begin
        mem_start_t <= preamb_cnt == 6'd29;
        mem_start_t_d <= mem_start_t;
    end

    always_ff @(posedge clk)
        if (state == IDLE)
            mem_start <= 1'b0;
        else if (mem_start_t)
            mem_start <= 1'b1;

    always_ff @(posedge clk)
        sop <= mem_start_t_d;

    assign eop = (raddr == {pkt_size, 2'b0});

    crc add_crc (
        .clk(clk),
        .rst(rst),
        .d(d_mem),
        .sop(sop),
        .eop(eop),
        .d_out(d_crc),
        .sop_out(),
        .eop_out(eop_crc));

    always_ff @(posedge clk) begin
        select_data <= preamb_cnt[5];
        select_data_d <= select_data;
        tx_d <= select_data_d ? d_crc : d_preamb;
    end

    always_ff @(posedge clk)
        eop_crc_d <= eop_crc;

    always_ff @(posedge clk or negedge rst) begin
        if (!rst)
            tx_en <= '0;
        else if (preamb_cnt == 5'd2)
            tx_en <= '1;
        else if (eop_crc_d)
            tx_en <= '0;
    end

endmodule

`default_nettype wire

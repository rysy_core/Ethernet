# Ethernet

Scapy code for experiments.

## Example Ethernet frame

```
p=Ether(src="00:00:00:00:00:00", dst="ff:ff:ff:ff:ff:ff")/Raw("biblioteka w Niniwie przez kota Aszurbanipala!")
len(p)
a=srp(p, iface="eth0")
a
a[0][0]
```

## Dump packet as hex

```
x = bytes_encode(p)
for t in x:
    print("%02X " % orb(t))
```

# Sniffer

```
for i in range(20):
    p = sniff(count=1, iface="eth0")
	hexdump(p[0])
	print("------------------------------------------------")
```

# LEDs control

```
import time
d=[0]*46
for i in range(8):
    d[0] = 2**i
	print("Send {}".format(i))
	sendp(Ether(dst="02:00:00:00:00:00", type=0x0101)/Raw(d), iface="eth0")
	time.sleep(0.1)
```

```
d[0] = 0
sendp(Ether(dst="02:00:00:00:00:01", type=0x0101)/Raw(d), iface="eth0")
```

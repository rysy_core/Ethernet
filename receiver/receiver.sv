/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module receiver (
    input wire clk,
    input wire rst,
    input wire [1:0]rxd,
    input wire crs_dv,
    output logic [1:0]txd,
    output logic tx_en,
    output [7:0]leds
);

    compare rx (
        .clk(clk),
        .rst(rst),
        .rx_d(rxd),
        .rx_en(crs_dv),
        .data(leds)
    );

endmodule

`default_nettype wire
